# BlinkToPy3
Discover Python 3 via carefully prepared Google colaboratory (Jupyter) notebooks

# Table of Contents

* [69 хвилин Python 3](http://bit.ly/69MinToPy3-ua). 
English version: [69 minutes to Python 3](http://bit.ly/69MinToPy3-en)
* [Поглиблені матеріали Python 3](http://bit.ly/Py3-advanced-ua). 
English version: [Python 3 Advanced](http://bit.ly/Py3-advanced-en)

## License

[Apache License 2.0](LICENSE)
